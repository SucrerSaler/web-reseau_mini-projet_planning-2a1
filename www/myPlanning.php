<?php
    session_start();
    $idUser = $_SESSION['idU'];
    $pseudoU = $_SESSION['pseudoU'];
?>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset=utf-8>
        <link rel="stylesheet" href="myPlanning.css">
        <title>Mon planning</title>
    </head>
    <body>
        <?php echo "<h2> Bonjour $pseudoU !</h2>"; ?>

        <!-- Le bloc suivant s'occupe de récuperer la date voulue (par défaut aujourd'hui, sinon la date du calendrier) -->
        <label for="date1">Planning du :</label>
        <form method="POST" action="myPlanning.php">
            <?php
            if (empty($_POST["targetDate"])) $targetDate = date("Y-m-d");
            else $targetDate = $_POST["targetDate"]; 
            echo "<input type='date' id='date1' name='targetDate' value='$targetDate'>";
            ?>
            
            <input type="submit" value="Valider">
            <br>
        </form>

        
        

        <!-- Le bloc suivant s'occupe de faire une insertion si le formulaire d'insertion a été submit (bouton "Ajouter") -->
        <?php
        require("connexionBD.php");
        require("insertPlanning.php");
        insertForm($targetDate);
        if (isset($_POST["activity"]) and !empty($_POST["hour"])) {
            $idA = (int)$_POST["activity"];
            $targetHour = (int)$_POST["hour"];
            insertPlanning($idUser, $idA, $targetDate, $targetHour);
        }
        require("tablePlanning.php");
        ?>
        </br>
        <form action="logout.php">
            <input type="submit" value="Déconnexion">
        </form>
        
    </body>
</html>