<?php
require("connexionBD.php");
session_start();

$pseudoU = $_POST['pseudo'];
$pwdU = $_POST['pwd'];

try {
    $connexion = connect_bd();
    $selec = "SELECT idU, pseudoU FROM USER WHERE pseudoU=:pseudoU and pwdU=:pwdU";
    $stmt = $connexion -> prepare($selec);
    $stmt -> bindParam(':pseudoU', $pseudoU);
    $stmt -> bindParam(':pwdU', $pwdU);
    $stmt -> execute();
    $array = $stmt->fetchALL();
    $rows = count($array);
    if($rows==1){
        $_SESSION['idU'] = $array[0]["idU"];
        $_SESSION['pseudoU'] = $array[0]["pseudoU"];
        header("Location: myPlanning.php");
    }else{
        $_SESSION['mess_err_log'] = "Le pseudo ou le mot de passe est incorrect.";
        header("Location: logingScreen.php");
    }

}
catch (PDOException $e) {
    printf("Échec connexion : %s\n", $e->getMessage());
}
?>