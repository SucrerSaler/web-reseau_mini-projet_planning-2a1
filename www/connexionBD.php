<?php

function connect_bd(){
    try{
        $connexion = new PDO('sqlite:/tmp/users.sqlite3');
        $connexion -> setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_WARNING);
    }
    catch(PDOException $e){
        printf("Échec connexion : %s\n", $e->getMessage());
        exit();
    }
	return $connexion;
}
?>