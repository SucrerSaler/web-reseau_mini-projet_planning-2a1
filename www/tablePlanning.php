<?php
$idUser = $_SESSION['idU'];
try {
    $connexion = connect_bd();
    $results = $connexion -> query("SELECT * from PARTICIPATE natural join ACTIVITY where idU = $idUser and dateA between date('$targetDate') and date('$targetDate', '+1 day')");

    $activities = array();
    for ($i = 8; $i < 21; ++$i) {
        $activities[$i] = NULL;
    }
    
    foreach ($results as $res) {
        $date = strtotime($res['dateA']);
        $hour = date("H", $date);
        $activities[(int)$hour] = $res;
    }

    echo "<table>";
    for ($i = 8; $i < 21; ++$i) {
        $act = $activities[$i];
        echo "<tr ";
        if (!empty($act)) {
            echo "class='$act[nomA]'><td>";
            echo $i;
            echo "h </td><td> $act[nomA]";
        }
        else echo "><td> ".$i."h </td><td> ";
        echo "</td></tr>";
    }
    echo "</table>";
}
catch(PDOException $ex) {
    echo $ex->getMessage();
}
?>