<?php
require("connexionBD.php");

try {
    $connexion = connect_bd();

    $users = array(
        array(
            "idU" => 0,
            "pseudoU" => "Michel",
            "pwdU" =>  "Azerty_123456"),
        array(
            "idU" => 1,
            "pseudoU" => "Pascal",
            "pwdU" =>  "Azerty_123456")
        );

    $activities = array(
        array("idA" => 0, "nomA" => "Java"),
        array("idA" => 1, "nomA" => "Python"),
        array("idA" => 2, "nomA" => "Anglais"),
        array("idA" => 3, "nomA" => "Repos"),
        array("idA" => 4, "nomA" => "Café"),
        array("idA" => 5, "nomA" => "PHP")
    );

    $participations = array(
        array("idU" => 0, "idA" => 0, "dateA" => "2021-10-18 10:00:00"),
        array("idU" => 1, "idA" => 0, "dateA" => "2021-10-18 10:00:00"),
        array("idU" => 0, "idA" => 1, "dateA" => "2021-10-18 16:00:00"),
        array("idU" => 1, "idA" => 2, "dateA" => "2021-10-18 17:00:00"),
        array("idU" => 0, "idA" => 3, "dateA" => "2021-10-19 14:00:00"),
        array("idU" => 1, "idA" => 4, "dateA" => "2021-10-19 14:00:00"),
        array("idU" => 1, "idA" => 0, "dateA" => "2021-10-20 10:00:00")
    );



    $insert = "INSERT INTO USER (idU, pseudoU, pwdU) VALUES (:idU, :pseudoU , :pwdU)";
    $stmt = $connexion -> prepare($insert);

    $stmt -> bindParam(':idU', $idU);
    $stmt -> bindParam(':pseudoU', $pseudoU);
    $stmt -> bindParam(':pwdU', $pwdU);

    foreach ($users as $user){
        $idU = $user['idU'];
        $pseudoU = $user['pseudoU'];
        $pwdU = $user['pwdU'];
        $stmt -> execute();
    }

    echo "Insertion des utilisateurs réussie. ";




    $insert = "INSERT INTO ACTIVITY (idA, nomA) VALUES (:idA, :nomA)";
    $stmt = $connexion -> prepare($insert);

    $stmt -> bindParam(':idA', $idA);
    $stmt -> bindParam(':nomA', $nomA);

    foreach ($activities as $act){
        $idA = $act['idA'];
        $nomA = $act['nomA'];
        $stmt -> execute();
    }

    echo "Insertion des activités réussie. ";




    $insert = "INSERT INTO PARTICIPATE (idU, idA, dateA) VALUES (:idU, :idA, :dateA)";
    $stmt = $connexion -> prepare($insert);

    $stmt -> bindParam(':idU', $idU);
    $stmt -> bindParam(':idA', $idA);
    $stmt -> bindParam(':dateA', $dateA);

    foreach ($participations as $part){
        $idU = $part['idU'];
        $idA = $part['idA'];
        $dateA = $part['dateA'];
        $stmt -> execute();
    }

    echo "Insertion des participations réussie. ";
}
catch (PDOException $e) {
    printf("Échec connexion : %s\n", $e->getMessage());
}


?>