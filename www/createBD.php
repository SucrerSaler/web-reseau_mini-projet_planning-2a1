<?php
require("connexionBD.php");

try {
    $connexion = connect_bd();

    $connexion -> exec("CREATE TABLE IF NOT EXISTS USER ( 
        idU INTEGER PRIMARY KEY,
        pseudoU TEXT,
        pwdU TEXT)");
    $connexion -> exec("CREATE TABLE IF NOT EXISTS ACTIVITY ( 
        idA INTEGER PRIMARY KEY,
        nomA TEXT)");
    $connexion -> exec("CREATE TABLE IF NOT EXISTS PARTICIPATE ( 
        idU INTEGER,
        idA INTEGER,
        dateA DATETIME,
        FOREIGN KEY (idU) REFERENCES USER (idU),
        FOREIGN KEY (idA) REFERENCES ACTIVITY (idA))");

    echo "Base de données créée.";
}
catch (PDOException $e) {
    printf("Échec connexion : %s\n", $e->getMessage());
}


?>